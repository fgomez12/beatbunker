<?php

namespace app\controllers;

use app\models\Herramientas;
use app\models\Utilizan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * HerramientasController implements the CRUD actions for Herramientas model.
 */
class HerramientasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
//        return array_merge( SE COMENTA ESTE RETURN PARA UTILIZAR LA geSTION DE USUARIOS DEL USER MANAGEMENT
//            parent::behaviors(),
//            [
//                'verbs' => [
//                    'class' => VerbFilter::className(),
//                    'actions' => [
//                        'delete' => ['POST'],
//                    ],
//                ],
//            ]
//        );
        return [
		'ghost-access'=> [
			'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
		],
	];
    }

    /**
     * Lists all Herramientas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Herramientas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idherramienta' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Herramientas model.
     * @param int $idherramienta Idherramienta
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idherramienta)
    {
        return $this->render('view', [
            'model' => $this->findModel($idherramienta),
        ]);
    }

    /**
     * Creates a new Herramientas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Herramientas();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    $idestudio = Yii::$app->request->post('idestudio');
                    if ($idestudio) {
                        $utilizan = new Utilizan();
                        $utilizan->idestudio = $idestudio;
                        $utilizan->idherramienta = $model->idherramienta;

                        if ($utilizan->save()) {
                            return $this->redirect(['create', 'idestudio' => $idestudio]);
                        } else {
                            Yii::error('Error al guardar la relación canción-instrumento: ' . print_r($cancionInstrumento->errors, true));
                        }
                    } else {
                        Yii::error('idCancion no recibido en el POST.');
                    }
                } else {
                    Yii::error('Error al guardar el instrumento: ' . print_r($model->errors, true));
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Herramientas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idherramienta Idherramienta
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idherramienta)
    {
        $model = $this->findModel($idherramienta);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idherramienta' => $model->idherramienta]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Herramientas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idherramienta Idherramienta
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idherramienta)
    {
        $this->findModel($idherramienta)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Herramientas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idherramienta Idherramienta
     * @return Herramientas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idherramienta)
    {
        if (($model = Herramientas::findOne(['idherramienta' => $idherramienta])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
