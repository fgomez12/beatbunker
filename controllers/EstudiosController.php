<?php

namespace app\controllers;

use app\models\Estudios;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * EstudiosController implements the CRUD actions for Estudios model.
 */
class EstudiosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all Estudios models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Estudios::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Estudios model.
     * @param int $idestudio Idestudio
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idestudio)
    {
        return $this->render('view', [
            'model' => $this->findModel($idestudio),
        ]);
    }

    /**
     * Creates a new Estudios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
{
    $model = new Estudios();

    if (Yii::$app->request->isPost) {
        if ($model->load(Yii::$app->request->post())) {
            $idcancion = Yii::$app->request->post('idcancion');
            $model->idcancion = $idcancion;

            if ($model->save()) {
                return $this->redirect( ['herramientas/create', 'idestudio' => $model->idestudio ] );
            } else {
                Yii::error('Failed to save model: ' . print_r($model->errors, true));
            }
        } else {
            Yii::error('Failed to load data into model');
        }
    } else {
        Yii::info('Request is NOT POST');
        $model->loadDefaultValues();
    }

    return $this->render('create', [
        'model' => $model,
    ]);
}

    /**
     * Updates an existing Estudios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idestudio Idestudio
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idestudio)
    {
        $model = $this->findModel($idestudio);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idestudio' => $model->idestudio]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Estudios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idestudio Idestudio
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idestudio)
    {
        $this->findModel($idestudio)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Estudios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idestudio Idestudio
     * @return Estudios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idestudio)
    {
        if (($model = Estudios::findOne(['idestudio' => $idestudio])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
