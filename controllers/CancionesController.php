<?php

namespace app\controllers;

use app\models\Canciones;
use app\models\Generos;  // Importamos el modelo generos
use app\models\Instrumentos; // Importamos el modelo Instrumentos
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

class CancionesController extends Controller
{
    public function behaviors()
    {
//        return array_merge( SE COMENTA ESTE RETURN PARA UTILIZAR LA geSTION DE USUARIOS DEL USER MANAGEMENT
//            parent::behaviors(),
//            [
//                'verbs' => [
//                    'class' => VerbFilter::className(),
//                    'actions' => [
//                        'delete' => ['POST'],
//                    ],
//                ],
//            ]
//        );
        return [
		'ghost-access'=> [
			'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
		],
	];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Canciones::find(),
        ]);

        $instrumentos = Instrumentos::find()->all(); // Obtenemos todos los instrumentos

    return $this->render('index', [
        'dataProvider' => $dataProvider,
        'instrumentos' => $instrumentos, // Pasamos la lista de instrumentos a la vista
    ]);
    }

    public function actionView($idcancion)
{
    $model = $this->findModel($idcancion); // Obtenemos el modelo Canciones correspondiente al ID
    $instrumentos = Instrumentos::find()->all(); // Obtenemos todos los instrumentos

    return $this->render('view', [
        'model' => $model, // Pasamos el modelo Canciones a la vista
        'instrumentos' => $instrumentos, // Pasamos la lista de instrumentos a la vista
    ]);
}

    public function actionCreate()    
    {
        Yii::warning('action create');
        $model = new Canciones();
        $generos = Generos::getGenerosList();

        if ($model->load(Yii::$app->request->post())) {
            $model->selectedGeneros = Yii::$app->request->post('Canciones')['selectedGeneros'];
            $model['idusuario'] = Yii::$app->user->id;
            Yii::warning('load' . $model['idusuario']);
            
            // Al crear una cancion redirige a la vista create del controla de instrumentoa 
            // y le pasa como parametro el idcancion
            if ($model->save()) {
                Yii::warning('save');
                return $this->redirect(['instrumentos/create', 'idcancion' => $model->idcancion]);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
            'generos' => $generos,
        ]);
    }
//    public function actionCreate()
//    {
//        Yii::warning('action create');
//        $model = new Canciones();
//        $generos = Generos::getGenerosList();
//        $instrumentos = Instrumentos::find()->where(['idusuario' => Yii::$app->user->id])->all();
//
//        if ($model->load(Yii::$app->request->post())) {
//            $model->selectedGeneros = Yii::$app->request->post('Canciones')['selectedGeneros'];
//            $model->idusuario = Yii::$app->user->id;
//
//            if ($model->save()) {
//                // Relacionar los instrumentos seleccionados con la canción
//                $selectedInstrumentos = Yii::$app->request->post('selectedInstrumentos', []);
//                foreach ($selectedInstrumentos as $idInstrumento) {
//                    $cancionInstrumento = new \app\models\CancionInstrumento();
//                    $cancionInstrumento->idcancion = $model->idcancion;
//                    $cancionInstrumento->idinstrumento = $idInstrumento;
//                    $cancionInstrumento->save();
//                }
//
//                return $this->redirect(['view', 'idcancion' => $model->idcancion]);
//            }
//        }
//
//        return $this->render('create', [
//            'model' => $model,
//            'generos' => $generos,
//            'instrumentos' => $instrumentos,
//        ]);
//    }

    public function actionUpdate($idcancion)
    {
        $model = $this->findModel($idcancion);
        $generos = Generos::getGenerosList();

        if ($model->load(Yii::$app->request->post())) {
            $model->selectedGeneros = Yii::$app->request->post('Canciones')['selectedGeneros'];
            if ($model->save()) {
                return $this->redirect(['view', 'idcancion' => $model->idcancion]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'generos' => $generos,
        ]);
    }

    public function actionDelete($idcancion)
    {
        $this->findModel($idcancion)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($idcancion)
    {
        if (($model = Canciones::findOne(['idcancion' => $idcancion])) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('La página solicitada no existe.');
    }
    
    public static function showUserName( $userId ) {
        
        return $userId;
    }
    
}
