<?php



namespace app\controllers;

use app\models\Generos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * GenerosController implementa las acciones CRUD para el modelo Generos.
 */
class GenerosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
//        return array_merge( SE COMENTA ESTE RETURN PARA UTILIZAR LA geSTION DE USUARIOS DEL USER MANAGEMENT
//            parent::behaviors(),
//            [
//                'verbs' => [
//                    'class' => VerbFilter::className(),
//                    'actions' => [
//                        'delete' => ['POST'],
//                    ],
//                ],
//            ]
//        );
        return [
		'ghost-access'=> [
			'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
		],
	]; 
    }

    /**
     * Lista todos los modelos de Generos.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Generos::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Muestra un solo modelo de Generos.
     * @param int $idgenero Idgenero
     * @return string
     * @throws NotFoundHttpException si no se puede encontrar el modelo
     */
    public function actionView($idgenero)
    {
        return $this->render('view', [
            'model' => $this->findModel($idgenero),
        ]);
    }

    /**
     * Crea un nuevo modelo de Generos.
     * Si la creación es exitosa, el navegador será redirigido a la página 'view'.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Generos();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idgenero' => $model->idgenero]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Actualiza un modelo existente de Generos.
     * Si la actualización es exitosa, el navegador será redirigido a la página 'view'.
     * @param int $idgenero Idgenero
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException si no se puede encontrar el modelo
     */
    public function actionUpdate($idgenero)
    {
        $model = $this->findModel($idgenero);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idgenero' => $model->idgenero]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Elimina un modelo existente de Generos.
     * Si la eliminación es exitosa, el navegador será redirigido a la página 'index'.
     * @param int $idgenero Idgenero
     * @return \yii\web\Response
     * @throws NotFoundHttpException si no se puede encontrar el modelo
     */
    public function actionDelete($idgenero)
    {
        $this->findModel($idgenero)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Encuentra el modelo de Generos basado en su valor de clave primaria.
     * Si no se encuentra el modelo, se lanzará una excepción HTTP 404.
     * @param int $idgenero Idgenero
     * @return Generos el modelo cargado
     * @throws NotFoundHttpException si no se puede encontrar el modelo
     */
    protected function findModel($idgenero)
    {
        if (($model = Generos::findOne(['idgenero' => $idgenero])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('La página solicitada no existe.');
    }
}
