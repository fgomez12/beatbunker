<?php

namespace app\controllers;

use app\models\Instrumentos;
use app\models\CancionInstrumento;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * InstrumentosController implements the CRUD actions for Instrumentos model.
 */
class InstrumentosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all Instrumentos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Instrumentos::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Instrumentos model.
     * @param int $idinstrumento Idinstrumento
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idinstrumento)
    {
        return $this->render('view', [
            'model' => $this->findModel($idinstrumento),
        ]);
    }

    /**
     * Creates a new Instrumentos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Instrumentos();
        $instrumentos = [
            1 => 'Guitarra',
            2 => 'Bajo',
            3 => 'Bateria',
            4 => 'Caja',
            5 => 'HiHat',
            6 => 'Crash',
            7 => 'Ride',
            8 => 'Teclado'
        ];

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                // Cambia índice seleccionado al nombre del instrumento
                if (isset($instrumentos[$model->tipo])) {
                    $model->tipo = $instrumentos[$model->tipo];
                }
                if ($model->save()) {
                    $idCancion = Yii::$app->request->post('idcancion');
                    if ($idCancion) {
                        $cancionInstrumento = new CancionInstrumento();
                        $cancionInstrumento->idcancion = $idCancion;
                        $cancionInstrumento->idinstrumento = $model->idinstrumento;

                        if ($cancionInstrumento->save()) {
                            Yii::info('Relación canción-instrumento guardada correctamente.');
                            return $this->redirect(['create', 'idcancion' => $idCancion]);
                        } else {
                            Yii::error('Error al guardar la relación canción-instrumento: ' . print_r($cancionInstrumento->errors, true));
                        }
                    } else {
                        return $this->redirect(['view']);
                    }
                } else {
                    Yii::error('Error al guardar el instrumento: ' . print_r($model->errors, true));
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'instrumentos' => $instrumentos,
        ]);
    }

    /**
     * Updates an existing Instrumentos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idinstrumento Idinstrumento
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idinstrumento)
    {
        $model = $this->findModel($idinstrumento);
        $instrumentos = [
            1 => 'Guitarra', 
            2 => 'Bajo', 
            3 => 'Bateria', 
            4 => 'Caja', 
            5 => 'HiHat',
            6 => 'Crash', 
            7 => 'Ride', 
            8 => 'Teclado'
        ];

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idinstrumento' => $model->idinstrumento]);
        }

        return $this->render('update', [
            'model' => $model,
            'instrumentos' => $instrumentos,
        ]);
    }

    /**
     * Deletes an existing Instrumentos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idinstrumento Idinstrumento
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idinstrumento)
    {
        $this->findModel($idinstrumento)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Instrumentos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idinstrumento Idinstrumento
     * @return Instrumentos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idinstrumento)
    {
        if (($model = Instrumentos::findOne(['idinstrumento' => $idinstrumento])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
