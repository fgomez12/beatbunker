<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Herramientas $model */

$this->title = 'Update Herramientas: ' . $model->idherramienta;
$this->params['breadcrumbs'][] = ['label' => 'Herramientas', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="herramientas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
