<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Herramientas $model */

$this->params['breadcrumbs'][] = ['label' => 'Herramientas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="herramientas-view">

    <h1>Detalle herramienta</h1>

    <p>
        <?php if( Yii::$app->user->identity->superadmin == 1 ) : ?>
            <?= Html::a('Editar', ['update', 'idherramienta' => $model->idherramienta], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        
        <?= Html::a('Listado', ['index'], ['class' => 'btn btn-primary']) ?>
        
        <?php if( Yii::$app->user->identity->superadmin == 1 ) : ?>
            <?= Html::a('Eliminar', ['delete', 'idherramienta' => $model->idherramienta], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'tipo',
            'descripcion:ntext',
            'url:url',
        ],
    ]) ?>

</div>
