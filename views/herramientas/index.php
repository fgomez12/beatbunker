<?php

use app\models\Herramientas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Herramientas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="herramientas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'tipo',
            'descripcion:ntext',
            'url:url',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Herramientas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idherramienta' => $model->idherramienta]);
                },
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return true; 
                    },
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->superadmin == 1;
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->identity->superadmin == 1;
                    },
                ],
            ],
        ],
    ]); ?>


</div>
