<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Estudios $model */

$this->params['breadcrumbs'][] = ['label' => 'Estudios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estudios-view">

    <h1>Detalle estudio</h1>

    <p>
        <?php if( Yii::$app->user->identity->superadmin == 1 ) : ?>
            <?= Html::a('Editar', ['update', 'idestudio' => $model->idestudio], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        
        <?= Html::a('Listado', ['index'], ['class' => 'btn btn-primary']) ?>
       
        <?php if( Yii::$app->user->identity->superadmin == 1 ) : ?>
            <?= Html::a('Eliminar', ['delete', 'idestudio' => $model->idestudio], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Seguro que quieres borrar este estudio?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif;?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'web',
            'tecnico_sonido',
        ],
    ]) ?>

</div>
