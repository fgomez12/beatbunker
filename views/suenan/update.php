<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Suenan $model */

$this->title = 'Update Suenan: ' . $model->idsuenan;
$this->params['breadcrumbs'][] = ['label' => 'Suenans', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="suenan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
