<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Instrumentos $model */
/** @var yii\widgets\ActiveForm $form */
/** @var array $instrumentos */

$request = Yii::$app->request;

$idcancion = $request->get('idcancion');
?>


<div class="instrumentos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tipo')->dropDownList($instrumentos, ['prompt' => 'Seleccione un tipo de instrumento']) ?>

    <?= $form->field($model, 'marca')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tamaño')->textInput() ?>

    <?= $form->field($model, 'material')->textInput(['maxlength' => true]) ?>
    
    <?= Html::hiddenInput('idcancion', $idcancion) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        <?php if($idcancion) : ?>
            <?= Html::a('Siguiente', ['estudios/create', 'idcancion' => $idcancion], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<!--//$form->field($model, 'tipo')->dropDownList($generos, ['prompt' => 'Seleccione un género'])-->