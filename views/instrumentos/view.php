<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Instrumentos $model */

$this->title = $model->idinstrumento;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="instrumentos-view">

    
    <p>
        <?php if( Yii::$app->user->identity->superadmin == 1 ) : ?>
            <?= Html::a('Editar', ['update', 'idinstrumento' => $model->idinstrumento], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?= Html::a('Terminar', ['index'], ['class' => 'btn btn-primary']) ?>
        <?php if( Yii::$app->user->identity->superadmin == 1 ) : ?>
            <?= Html::a('Eliminar', ['delete', 'idinstrumento' => $model->idinstrumento], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idinstrumento',
            'tipo',
            'marca',
            'modelo',
            'tamaño',
            //'material',
            //'url:url',
        ],
    ]) ?>

</div>
