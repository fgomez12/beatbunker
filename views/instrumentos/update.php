<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Instrumentos $model */
/** @var array $instrumentos */

$this->title = 'Modificar Intrumento: ' . $model->tipo;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="instrumentos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'instrumentos' => $instrumentos, // Pasando la variable instrumentos
    ]) ?>

</div>

