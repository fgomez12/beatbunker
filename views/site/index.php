<?php
use yii\helpers\Html;
use yii\grid\GridView;

/** @var yii\web\View $this */
/* @var $searchModel app\models\UsuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'My Yii Application';
//$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="body-content d-flex align-items-center justify-content-center custom-height">
    <div class="row justify-content-center w-100">
        <a class="col-md-5 col-sm-10 mb-4 card custom-card" href="index.php/canciones/index">
            <div class="card-content btn">
                <div class="image-container">
                    <img src="<?= Yii::getAlias('@web') ?>/images/explorar.png" alt="Explorar">
                </div>
                <div class="card-text">
                    <h2 class="card-title">EXPLORAR</h2>
                </div>
            </div>
        </a>
        <a class="col-md-5 col-sm-10 mb-4 card custom-card" href="index.php/canciones/create">
            <div class="card-content btn">
                <div class="image-container">
                    <img src="<?= Yii::getAlias('@web') ?>/images/crear.png" alt="Crear">
                </div>
                <div class="card-text">
                    <h2 class="card-title">CREAR</h2>
                </div>
            </div>
        </a>            
    </div>
</div>


