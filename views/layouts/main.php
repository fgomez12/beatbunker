<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use webvimark\modules\UserManagement\UserManagementModule;
use webvimark\modules\UserManagement\models\rbacDB\Role;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="images/favicon.ico" type="image/icon">
    
    <?php $this->registerCsrfMetaTags() ?>
    

    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/logoCuadrado.jpg', ['alt' => Yii::$app->name, 'style' => 'max-height:40px;']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-black fixed-top beatbunker-mobile', // Modificado a navbar-black
        ],
        ]);

    // Verificación del rol del usuario
    if (!Yii::$app->user->isGuest && Yii::$app->user->identity->username === 'superadmin') {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav beatbunker-nav'],
            'items' => [
                ['label' => 'Canciones', 'url' => ['/canciones/index']],
                ['label' => 'Instrumentos', 'url' => ['/instrumentos/index']],
                ['label' => 'Generos', 'url' => ['/generos/index']],
                ['label' => 'Estudios', 'url' => ['/estudios/index']],
                ['label' => 'Herramientas', 'url' => ['/herramientas/index']],
                [
                    'label' => 'Gestión de permisos',
                    'items' => UserManagementModule::menuItems()
                ],
                [
                    'label' => 'Menú Sesión',
                    'items' => [
                        ['label' => 'Salir', 'url' => ['/user-management/auth/logout']],                        
                        ['label' => 'Cambiar contraseña', 'url' => ['/user-management/auth/change-own-password']],
                   
                    ],
                ],
            ],
        ]);
    } else {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav beatbunker-nav'],
            'items' => [
                ['label' => 'Canciones', 'url' => ['/canciones/index']],
                ['label' => 'Instrumentos', 'url' => ['/instrumentos/index']],           
                ['label' => 'Estudios', 'url' => ['/estudios/index']],
                ['label' => 'Herramientas', 'url' => ['/herramientas/index']],
                [
                    'label' => 'Menú Sesión',
                    'items' => [
                        
                        ['label' => 'Salir', 'url' => ['/user-management/auth/logout']],
                        ['label' => 'Cambiar contraseña', 'url' => ['/user-management/auth/change-own-password']],
                      
                    ],
                ],
            ],
        ]);
    }

    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0 site-main">
    <div class="container">
        
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; BeatBunker <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
