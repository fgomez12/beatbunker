<?php

use app\models\Generos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Generos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="generos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Generos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'idgenero',            
            'genero',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Generos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idgenero' => $model->idgenero]);
                 }
            ],
        ],
    ]); ?>


</div>
