<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Canciones $model */

// Establece el título de la página como el ID de la canción actual


// Añade enlaces de migas de pan para navegación
$this->params['breadcrumbs'][] = ['label' => 'Canciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// Registra los activos necesarios para la vista
\yii\web\YiiAsset::register($this);
?>

<div class="canciones-view">

    <!-- Muestra el título de la página -->
    <h1>Detalle de canción</h1>

    <p>
        <!-- Verifica si el usuario actual es el propietario de la canción o un superadmin -->
        <?php if( Yii::$app->user->id == $model->idusuario || Yii::$app->user->identity->superadmin == 1 ) : ?>
            <!-- Muestra el botón de editar si el usuario tiene permisos -->
            <?= Html::a('Editar', ['update', 'idcancion' => $model->idcancion], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>

        <!-- Muestra el botón para regresar al listado de canciones -->
        <?= Html::a('Listado', ['index'], ['class' => 'btn btn-primary']) ?>
        
        <!-- Verifica nuevamente si el usuario es el propietario o un superadmin -->
        <?php if( Yii::$app->user->id == $model->idusuario || Yii::$app->user->identity->superadmin == 1 ) : ?>
            <!-- Muestra el botón de eliminar si el usuario tiene permisos, con confirmación -->
            <?= Html::a('Eliminar', ['delete', 'idcancion' => $model->idcancion], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Seguro que quieres borrar esta canción?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <!-- Muestra un widget de detalle para visualizar los atributos del modelo de la canción -->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'titulo',
            'album',
            'url_video:url',
            'interprete',
            [
                'label' => 'Generos',
                'value' => implode(\yii\helpers\ArrayHelper::map($model->generos, 'idgenero', 'genero')), 
            ],
            [
                'label' => 'Instrumentos',
                'value' => function ($model) {
                    $instrumentos = $model->instrumentos;
                    $instrumentInfo = '';
                    foreach ($instrumentos as $instrumento) {
                        $instrumentInfo .= ' ' . Html::encode($instrumento->tipo) . ': ' .
                                           ' ' . Html::encode($instrumento->marca) . ', ' .
                                           ' ' . Html::encode($instrumento->modelo) . ' ' .
                                           ' ' . Html::encode($instrumento->tamaño) . ', ' .
                                           'Material: ' . Html::encode($instrumento->material ) . ', ' ;
                    }
                    $output = $instrumentInfo ;
                    return $output;
                }
            ]
            
        ],
    ]) ?>

</div>
