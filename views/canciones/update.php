<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Canciones $model */
/** @var array $generos */

$this->title = 'Modificar Canción: ' . $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Canciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="canciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'generos' => $generos, // Pasando la variable generos
    ]) ?>

</div>

