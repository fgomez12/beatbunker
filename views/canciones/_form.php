<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Canciones $model */
/** @var array $generos */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="canciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'album')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'url_video')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'interprete')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'selectedGeneros')->listBox($generos, ['multiple' => true]) ?>
    
    

    <div class="form-group">
        <?= Html::submitButton('Añadir', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
   

</div>
