<?php

use app\models\Canciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Canciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="canciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Cancion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'idusuario',
                'label' => 'Usuario',
                'value' => function($model) {
                    return Html::encode($model->getUsername());
                }
            ],

            [
                'attribute' => 'interprete_titulo_album',
                'label' => 'Interprete / Titulo / Album / Instrumentos',
                'format' => 'raw',
                'value' => function($model) {
                    $output = '<span class="interprete">' . Html::encode($model->interprete) . '</span><br>' .
                              '<span class="titulo">' . Html::encode($model->titulo) . '</span><br>' .
                              '<span class="album">' . Html::encode($model->album) . '</span>';
                    $output .= '<br><br><strong>Instrumentos:</strong><br>';
                    $instrumentos = $model->instrumentos;
                    $instrumentInfo = '';
                    foreach ($instrumentos as $instrumento) {
                        $instrumentInfo .= ' ' . Html::encode($instrumento->tipo) . ': ' .
                                           ' ' . Html::encode($instrumento->marca) . ', ' .
                                           ' ' . Html::encode($instrumento->modelo) . ' ' .
                                           ' ' . Html::encode($instrumento->tamaño) . ', ' .
                                           'Material: ' . Html::encode($instrumento->material) . ', ' ;
                    }
                    $output .= '<span class="instrumentos" style="font-size: 0.9em;">' . $instrumentInfo . '</span>';
                    return $output;
                },
                'headerOptions' => ['style' => 'width:40%'],
                'contentOptions' => ['style' => 'vertical-align: top;'],
            ],

            [
                'attribute' => 'url_video',
                'format' => 'raw',
                'headerOptions' => ['style' => 'width:50%'],
                'contentOptions' => ['style' => 'vertical-align: top;'],
                'value' => function($model) {
                    $videoURL = $model->url_video;
                    $convertedURL = str_replace("watch?v=", "embed/", $videoURL);
                    return '<div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="' . $convertedURL . '" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>';
                },
            ],

            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Canciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idcancion' => $model->idcancion]);
                },
                // Aquí mostramos los botones que queremos que se vean y la logica de permisos para los mismos
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return true; 
                    },
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->id == $model->idusuario || Yii::$app->user->identity->superadmin == 1;
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->id == $model->idusuario || Yii::$app->user->identity->superadmin == 1;
                    },
                ],
            ],
        ],
    ]); ?>
</div>
