<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utilizan".
 *
 * @property int $idutilizan
 * @property int $idestudio
 * @property int $idherramienta
 *
 * @property Estudios $idestudio0
 * @property Herramientas $idherramienta0
 */
class Utilizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'utilizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idestudio', 'idherramienta'], 'required'],
            [['idestudio', 'idherramienta'], 'integer'],
            [['idestudio'], 'exist', 'skipOnError' => true, 'targetClass' => Estudios::class, 'targetAttribute' => ['idestudio' => 'idestudio']],
            [['idherramienta'], 'exist', 'skipOnError' => true, 'targetClass' => Herramientas::class, 'targetAttribute' => ['idherramienta' => 'idherramienta']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idutilizan' => 'Idutilizan',
            'idestudio' => 'Idestudio',
            'idherramienta' => 'Idherramienta',
        ];
    }

    /**
     * Gets query for [[Idestudio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdestudio0()
    {
        return $this->hasOne(Estudios::class, ['idestudio' => 'idestudio']);
    }

    /**
     * Gets query for [[Idherramienta0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdherramienta0()
    {
        return $this->hasOne(Herramientas::class, ['idherramienta' => 'idherramienta']);
    }
}
