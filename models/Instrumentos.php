<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instrumentos".
 *
 * @property int $idinstrumento
 * @property string $tipo
 * @property string|null $marca
 * @property string|null $modelo
 * @property float|null $tamaño
 * @property string|null $material
 * @property string|null $url
 * @property int $idusuario
 *
 * @property User $usuario
 * @property Suenan[] $suenans
 * @property CancionInstrumento[] $cancionInstrumentos
 * @property Canciones[] $canciones
 */
class Instrumentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instrumentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tamaño'], 'number'],
            [['tipo', 'material'], 'string', 'max' => 255],
            [['marca', 'modelo'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idinstrumento' => 'ID Instrumento',
            'tipo' => 'Tipo',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'tamaño' => 'Tamaño',
            'material' => 'Material',
        ];
    }

    /**
     * Gets query for [[Suenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSuenans()
    {
        return $this->hasMany(Suenan::class, ['idinstrumento' => 'idinstrumento']);
    }

    /**
     * Gets query for [[Canciones]].
     * 
     * @return \yii\db\ActiveQuery
     */
    public function getCanciones()
    {
        return $this->hasMany(Canciones::class, ['idcancion' => 'idcancion'])
                    ->viaTable('cancion_instrumento', ['idinstrumento' => 'idinstrumento']);
    }
    
    public function getCancionInstrumentos()
    {
        return $this->hasMany(CancionInstrumento::class, ['idinstrumento' => 'idinstrumento']);
    }
}
