<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

class Generos extends ActiveRecord
{
    public static function tableName()
    {
        return 'generos';
    }

    public function rules()
    {
        return [
            [['genero'], 'required'],
            [['genero'], 'string', 'max' => 50],
        ];
    }

    public function attributeLabels()
    {
        return [
            'idgenero' => 'Idgenero',
            'genero' => 'Genero',
        ];
    }

    public static function getGenerosList()
    {
        return ArrayHelper::map(self::find()->all(), 'idgenero', 'genero');
        
    }
}
