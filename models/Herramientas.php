<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "herramientas".
 *
 * @property int $idherramienta
 * @property string $nombre
 * @property string $tipo
 * @property string|null $descripcion
 * @property string|null $url
 *
 * @property Utilizan[] $utilizans
 */
class Herramientas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'herramientas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'tipo'], 'required'],
            [['tipo', 'descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 100],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idherramienta' => 'Idherramienta',
            'nombre' => 'Nombre',
            'tipo' => 'Tipo',
            'descripcion' => 'Descripcion',
            'url' => 'Url',
        ];
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::class, ['idherramienta' => 'idherramienta']);
    }
    
    public function getEstudios()
    {
        return $this->hasMany(Estudios::class, ['idestudio' => 'idestudio'])
                    ->viaTable('utilizan', ['idherramienta' => 'idherramienta']);
    }
    
//    public function getCancionInstrumentos()
//    {
//        return $this->hasMany(CancionInstrumento::class, ['idinstrumento' => 'idinstrumento']);
//    }
}
