<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "canciones".
 *
 * @property int $idcancion
 * @property string $titulo
 * @property string $album
 * @property string $url_video
 * @property string|null $interprete
 * @property int $idusuario
 *
 * @property Estudios[] $estudios
 * @property Generos[] $generos
 * @property User $idusuario0
 * @property Suenan[] $suenans
 * @property Instrumento[] $instrumentos
 * @property CancionInstrumento[] $cancionInstrumentos
 */
class Canciones extends \yii\db\ActiveRecord
{
    public $selectedGeneros = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'canciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'album', 'url_video', 'idusuario'], 'required'],
            [['idusuario'], 'integer'],
            [['titulo', 'album', 'url_video'], 'string', 'max' => 255],
            [['interprete'], 'string', 'max' => 45],
            [['selectedGeneros'], 'safe'],
            [['idusuario'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['idusuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcancion' => 'Idcancion',
            'titulo' => 'Titulo',
            'album' => 'Album',
            'url_video' => 'Url Video',
            'interprete' => 'Interprete',
            'idusuario' => 'Usuario',
            'selectedGeneros' => 'Géneros',
        ];
    }

    /**
     * Gets query for [[Estudios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudios()
    {
        return $this->hasMany(Estudios::class, ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Generos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeneros()
    {
        return $this->hasMany(Generos::class, ['idgenero' => 'idgenero'])
            ->viaTable('cancion_genero', ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Idusuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdusuario0()
    {
        return $this->hasOne(User::class, ['id' => 'idusuario']);
    }
    
    /**
     * Retorna el nombre de usuario asociado a la canción
     */
    public function getUsername()
    {
        if( $this ->idusuario ) {
            return User::getUsername($this->idusuario);
        }
        
        return "Invitado";
    }

    /**
     * Gets query for [[Suenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSuenans()
    {
        return $this->hasMany(Suenan::class, ['idcancion' => 'idcancion']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->updateGeneros();
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->selectedGeneros = $this->getGeneros()->select('idgenero')->column();
    }

    public function updateGeneros()
    {
        $currentGenreIds = array_map(function ($genre) {
            return $genre->idgenero;
        }, $this->generos);

        $newGenreIds = $this->selectedGeneros ?: [];

        $toDelete = array_diff($currentGenreIds, $newGenreIds);
        $toAdd = array_diff($newGenreIds, $currentGenreIds);

        if (!empty($toDelete)) {
            Yii::$app->db->createCommand()
                ->delete('cancion_genero', ['idcancion' => $this->idcancion, 'idgenero' => $toDelete])
                ->execute();
        }

        if (!empty($toAdd)) {
            foreach ($toAdd as $idgenero) {
                Yii::$app->db->createCommand()
                    ->insert('cancion_genero', ['idcancion' => $this->idcancion, 'idgenero' => $idgenero])
                    ->execute();
            }
        }
        
    }

    public static function getGenerosList()
    {
        return ArrayHelper::map(Generos::find()->all(), 'idgenero', 'genero');
    }

    // Definimos una relación muchos a muchos entre Canciones e Instrumentos
    public function getInstrumentos()
    {
        return $this->hasMany(Instrumentos::class, ['idinstrumento' => 'idinstrumento'])
                    ->viaTable('cancion_instrumento', ['idcancion' => 'idcancion']);
    }
    
    public function getCancionInstrumentos()
    {
        return $this->hasMany(CancionInstrumento::class, ['idcancion' => 'idcancion']);
    }
}
