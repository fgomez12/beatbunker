<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudios".
 *
 * @property int $idestudio
 * @property string $nombre
 * @property string|null $web
 * @property string|null $tecnico_sonido
 * @property int $idcancion
 *
 * @property Canciones $idcancion0
 * @property Utilizan[] $utilizans
 */
class Estudios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'idcancion'], 'required'],
            [['idcancion'], 'integer'],
            [['nombre', 'web'], 'string', 'max' => 255],
            [['tecnico_sonido'], 'string', 'max' => 70],
            [['idcancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::class, 'targetAttribute' => ['idcancion' => 'idcancion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idestudio' => 'Idestudio',
            'nombre' => 'Nombre',
            'web' => 'Web',
            'tecnico_sonido' => 'Tecnico Sonido',
            'idcancion' => 'Id Cancion',
        ];
    }

    /**
     * Gets query for [[Idcancion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcancion0()
    {
        return $this->hasOne(Canciones::class, ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::class, ['idestudio' => 'idestudio']);
    }
    
    public function getHerramientas()
    {
        return $this->hasMany(Herramientas::class, ['idherramienta' => 'idherramienta'])
                    ->viaTable('utilizan', ['idestudio' => 'idestudio']);
    }
}

