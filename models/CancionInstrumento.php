<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cancion_instrumento".
 *
 * @property int $id
 * @property int $idcancion
 * @property int $idinstrumento
 *
 * @property Canciones $idcancion0
 * @property Instrumentos $idinstrumento0
 */
class CancionInstrumento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cancion_instrumento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcancion', 'idinstrumento'], 'required'],
            [['idcancion', 'idinstrumento'], 'integer'],
            [['idcancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::class, 'targetAttribute' => ['idcancion' => 'idcancion']],
            [['idinstrumento'], 'exist', 'skipOnError' => true, 'targetClass' => Instrumentos::class, 'targetAttribute' => ['idinstrumento' => 'idinstrumento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idcancion' => 'Idcancion',
            'idinstrumento' => 'Idinstrumento',
        ];
    }

    /**
     * Gets query for [[Idcancion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcancion0()
    {
        return $this->hasOne(Canciones::class, ['idcancion' => 'idcancion']);
    }

    /**
     * Gets query for [[Idinstrumento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdinstrumento0()
    {
        return $this->hasOne(Instrumentos::class, ['idinstrumento' => 'idinstrumento']);
    }
}
