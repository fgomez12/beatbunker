<?php
/**
 * @var $this yii\web\View
 * @var $model webvimark\modules\UserManagement\models\forms\LoginForm
 */

use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
?>

<div class="container" id="login-wrapper">
    <div class="row no-gutters">
        <div class="col-md-6 left-half">
            <div class="image-container">
                <img src="<?= Yii::getAlias('@web') ?>/images/logo2.jpg" alt="Logo main">
               
            </div>
        </div>
        <div class="col-md-6 right-half">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= UserManagementModule::t('front', 'Login') ?></h3>
                </div>

                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'options' => ['autocomplete' => 'off'],
                    'validateOnBlur' => false,
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                    ],
                ]) ?>

                <?= $form->field($model, 'username')
                    ->textInput(['placeholder' => $model->getAttributeLabel('username'), 'autocomplete' => 'off']) ?>

                <?= $form->field($model, 'password')
                    ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autocomplete' => 'off']) ?>

                <?= (isset(Yii::$app->user->enableAutoLogin) && Yii::$app->user->enableAutoLogin) ? $form->field($model, 'rememberMe')->checkbox(['value' => true]) : '' ?>

                <?= Html::submitButton(
                    UserManagementModule::t('front', 'Login'),
                    ['class' => 'btn btn-lg btn-block btn-login']
                ) ?>

                <div class="row registration-block">
                    <div class="col-sm-6">
                        <?= Html::a(
                            UserManagementModule::t('front', "Registration"),
                            ['/user-management/auth/registration']
                        ) ?>
                    </div>
<!--                    <div class="col-sm-6 text-right">
                        <?= Html::a(
                            UserManagementModule::t('front', "Forgot password ?"),
                            ['/user-management/auth/password-recovery']
                        ) ?>
                    </div>-->
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$css = <<<CSS
@import url('https://fonts.googleapis.com/css2?family=Genos:wght@300&display=swap');

html, body {
    font-family: 'Genos', sans-serif;
    background-color: #010305; /* Fondo negro */
    color: #ffff00; /* Texto amarillo */
    height: 100%;
    margin: 0;
    padding: 0;
}

.container {
    display: flex;
    height: 100vh;
    width: 100vw;
}

.left-half, .right-half {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100vh;
    width: 50%;
}

.left-half {
    background-color: #010305;
    color: #ffff00;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden; /* Asegura que la imagen no se salga del contenedor */
}

.image-container img {
    width: 100%; /* Asegura que la imagen ocupe todo el ancho del contenedor */
    height: 100%; /* Asegura que la imagen ocupe toda la altura del contenedor */
    object-fit: cover; /* Mantiene la proporción de la imagen */
}

.right-half {
    background-color: #010305;
    color: #ffff00;
    display: flex;
    align-items: center;
    justify-content: center;
}

.panel {
    background-color: #1c1c1c;
    padding: 20px;
    border-radius: 8px;
    width: 100%;
    max-width: 400px;
}

.panel-heading {
    margin-bottom: 15px;
}

.btn-login {
    background-color: #ffff00;
    border: none;
    color: #000;
    padding: 10px 20px;
    cursor: pointer;
    font-size: 18px;
}

.btn-login:hover {
    background-color: #e6e600;
}

.registration-block {
    margin-top: 15px;
}

.registration-block a {
    color: #ffff00;
}

.registration-block a:hover,
.registration-link:hover {
    color: #ffff00;
}
}
.checkbox-yellow input[type="checkbox"] {
    accent-color: #ffff00;
}

.checkbox-yellow label {
    color: #ffff00;
}
CSS;

$this->registerCss($css);
?>
